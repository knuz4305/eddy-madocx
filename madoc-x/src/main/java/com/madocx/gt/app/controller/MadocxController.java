/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.madocx.gt.app.controller;

import com.madocx.gt.app.dto.InformacionDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author ervin
 */
@Controller
@RequestMapping("/api")
public class MadocxController {
    
    @PostMapping("/guardar")
    public String guardarContenido(InformacionDto informacionDto){
        return "result";
    }
    
    @PostMapping("/upload_file")
    public String uploadFile(){
        System.out.println("llego hasta este punto");
        return "index";
    }
    
    @RequestMapping("/")
    public String welcome(Model model) {
        model.addAttribute("informacionDto", new InformacionDto());
        return "index";
    }
    
}
