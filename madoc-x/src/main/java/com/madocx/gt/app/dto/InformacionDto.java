/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.madocx.gt.app.dto;

import java.io.Serializable;

/**
 *
 * @author ervin
 */
public class InformacionDto implements Serializable{
    
    public static final Long serialVersionUID = 1L;
    
    private String contenido;

    public InformacionDto() {
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
    
}
