package com.madocx.gt.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaDocXApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaDocXApplication.class, args);
	}
}
